define(["jquery"], function (jQuery) {

//noinspection BadExpressionStatementJS
+function ($) {
    'use strict';

    var backTop = function () {
        jQuery('body').append('<div class="'+$('body').attr('data-back-top-class')+'" id="back-top-button"></div>');
    };
    backTop.prototype.top = function () {
        backTop.call(this);
        var currentPosition;
        $(document).scroll(function () {
            currentPosition = $(document).scrollTop();
            if (currentPosition > 400) {
                jQuery('#back-top-button').fadeIn(500);
            }
            else {
                jQuery('#back-top-button').fadeOut(300);
            }
        });
        jQuery('#back-top-button').click(function () {
            jQuery('body,html').animate({
                scrollTop: 0
            }, (currentPosition/3) );
        });
    };
    $(document).one('scroll.aslanoba.goback.data-api', function () {
        if ((jQuery(document).height() / 1.2) > jQuery(window).height()) {
            backTop.prototype.top();
        }
    });
}(jQuery);


});